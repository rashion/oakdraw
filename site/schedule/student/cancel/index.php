<?php
    // $jsonpost = $_POST['student_message'];
    $jsonpost = file_get_contents('php://input');

    $datapost = json_decode($jsonpost);

    $message = $datapost->student_message;

    if (empty($message)) {
        http_response_code(400);
        die("Error reading Student Message");
    }
 
    $data = (object) array(
        "id" => $message->student_id,
        "type" => $message->type,
        "trip" => $message->trip,
        "message" => $message->message
    );

    $timestamp = time(); 
    $event = [$timestamp => $data];
    $jsonevent = json_encode($event);
    
    event_write($timestamp, $jsonevent);
    event_post($jsonevent);

    function event_write($time, $event) {
        $date = date("Y-m-d", $time);

        $filename = "../../../events/".$date;
        $file = fopen($filename, "a+") or die("Unable to open event file!");

        $data = $event."\n";

        fwrite($file, $data);
        fclose($file);
    }

    function event_post($event) {
        // Setup cURL
        $ch = curl_init('https://dubovozka-notification.herokuapp.com/store');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => $event
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            http_response_code(400);
            die("Operation POST failed!\n".$response."\n".curl_error($ch)."\n".curl_getinfo($ch));
        }

        // Close cURL
        curl_close($ch);
    }
?>