<html>

<head>
    <link rel="stylesheet" href="../styles/green-light.css">
    <style>
        p {
            text-align: center;
            color: #111111;
            font-size: 20px;
            margin: auto;
            padding: 4px;
        }
    </style>
</head>

<body>
    <div>
        <a aling=left href="../index.php">
            <h1 align=left>
                <img src="../images/oak-leaf_150.png">
                Dubovozka
            </h1>
        </a>
        <h2 align=right style='text-align:right;padding-right:10%;'>
            <a aling=right style='color:inherit;' href="index.php?direction=To">
                To
            </a>
            /
            <a aling=right style='color:inherit;' href="index.php?direction=From">
                From
            </a>
        </h2>
        <?php

        $filename = 'schedule.json';
        $data = file_get_contents($filename);
        $schedule = json_decode($data, true);

        // $direction = array_keys($schedule);
        $direction = $_GET['direction'];
        if (empty($direction)) $direction = "To";

        echo "<table align=center>";

        // for ($x = 0; $x < count($schedule); $x++) {
        echo "<tr style='background:#84c283;'>
                    <td colspan=3 valign=top>
                        <h3 align=center>" . $direction . "</h3>
                    </td>
                    </tr>";
        $days = array_keys($schedule[$direction]);
        for ($y = 0; $y < count($schedule[$direction]); $y++) {
            echo "<tr style='background:#808080'>
                        <td colspan=3 valign=top>
                            <h4 align=center>" . $days[$y] . "</h4>
                        </td>
                        </tr>";
            $max = max_st($schedule[$direction][$days[$y]]);

            $station = array_keys($schedule[$direction][$days[$y]]);
            echo "  <tr style='background:#808080'>
                    ";

            for ($z = 0; $z < count($station); $z++) {
                echo "  <td valign=top>
                                    <h5 align=center>" . $station[$z] . "</h5>
                                </td>
                                ";
            }
            echo "  </tr>
                        ";
            for ($r = 0; $r < $max; $r++) {
                fill_row($schedule[$direction][$days[$y]], $direction[$x], $days[$y], $r);
            }
        }
        // }

        echo "</table>";

        function max_st(array $array)
        {
            $max = 0;
            $station = array_keys($array);
            for ($i = 0; $i < count($array); $i++) {
                if ($max < count($array[$station[$i]])) {
                    $max = count($array[$station[$i]]);
                }
            }
            return $max;
        }

        function fill_cell(array $array, int $index)
        {
            echo "<td valign=top>
                        <p align=center>";
            if (isset($array[$index])) {
                echo $array[$index]["start"] . "</p>
                    </td>";
            }
        }

        function fill_row(array $array, string $direction, string $days, int $index)
        {
            echo "<tr>
                ";
            $station = array_keys($array);
            for ($i = 0; $i < count($station); $i++) {
                fill_cell($array[$station[$i]], $index);
            }
            echo "</tr>
                ";
        }

        ?>
        <br><br><br><br>
    </div>
</body>

</html>