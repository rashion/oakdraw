<html>

<head>
    <link rel="stylesheet" href="../../styles/green-light.css">
    <style>
    </style>
</head>

<body>
    <div>
        <a aling=left href="../../index.php">
            <h1 align=left>
                <img src="../../images/oak-leaf_150.png">
                Dubovozka
            </h1>
        </a>
        <form action="write.php" method="post">
            <?php

            $filename = '../schedule.json';
            $data = file_get_contents($filename);
            $schedule = json_decode($data, true);

            $direction = array_keys($schedule);

            echo "<table align=center>";
            
            for ($x = 0; $x < count($schedule); $x++) {
                echo "<tr style='background:#84c283;'>
                    <td colspan=3 valign=top>
                        <h3 align=center>" . $direction[$x] . "</h3>
                    </td>
                    </tr>";
                $days = array_keys($schedule[$direction[$x]]);
                for ($y = 0; $y < count($schedule[$direction[$x]]); $y++) {
                    echo "<tr style='background:#808080'>
                        <td colspan=3 valign=top>
                            <h4 align=center>" . $days[$y] . "</h4>
                        </td>
                        </tr>";
                    $max = max_st($schedule[$direction[$x]][$days[$y]]);

                    $station = array_keys($schedule[$direction[$x]][$days[$y]]);
                    echo "  <tr style='background:#808080'>
                    ";

                    for ($z = 0; $z < count($station); $z++) {    
                        echo "  <td valign=top>
                                    <h5 align=center>".$station[$z]."</h5>
                                </td>
                                ";
                    }
                    echo "  </tr>
                        ";
                    for ($r = 0; $r <= $max; $r++) {
                        fill_row($schedule[$direction[$x]][$days[$y]], $direction[$x], $days[$y], $r);
                    }
                }
            }

            echo "</table>";

            function max_st(array $array)
            {
                $max = 0;
                $station = array_keys($array);
                for ($i = 0; $i < count($array); $i++) {
                    if ($max < count($array[$station[$i]])) {
                        $max = count($array[$station[$i]]);
                    }
                }
                return $max;
            }

            function fill_cell(array $array, string $direction, string $days, string $station, int $index)
            {
                echo "<td valign=top>
                    ";
                echo '<input type="time" name="schedule[' . $direction . '][' . $days . '][' . $station . '][' . $index . '][start]" value="';
                if (isset($array[$index])) {
                    echo $array[$index]["start"];
                }
                echo '">
                </td>
                ';
            }

            function fill_row(array $array, string $direction, string $days, int $index)
            {
                echo "<tr>
                ";
                $station = array_keys($array);
                for ($i = 0; $i < count($station); $i++) {
                    fill_cell($array[$station[$i]], $direction, $days, $station[$i], $index);
                }
                echo "</tr>
                ";
            }

            ?>
            <br>
            <p><input type="submit"></p>
        </form>
        <p align=center></p>
    </div>
</body>

</html>