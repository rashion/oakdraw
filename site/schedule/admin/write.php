<?php
    $data = $_POST["schedule"];

    $direction = array_keys($data);
    for ($x = 0; $x < count($data); $x++) {
        $days = array_keys($data[$direction[$x]]);
        for ($y = 0; $y < count($data[$direction[$x]]); $y++) {
            $station = array_keys($data[$direction[$x]][$days[$y]]);
            for ($z = 0; $z < count($data[$direction[$x]][$days[$y]]); $z++) {
                remove_empty($data[$direction[$x]][$days[$y]][$station[$z]]);
                sort($data[$direction[$x]][$days[$y]][$station[$z]]);
            }
        }
    }

    $filename = "../schedule.json";
    $jsondata = json_encode($data);
    $shasum_file = sha1_file($filename);
    $shasum_data = sha1($jsondata);

    if ($shasum_file != $shasum_data) {
        // Writing schedule file
        $file = fopen($filename, "w") or die("Unable to open schedule file!");
        fwrite($file, $jsondata);
        fclose($file);

        $command = 'cd /var/www/html/dubovozka/schedule && sha256sum ./schedule.json > ./schedule.json.sha256';
        shell_exec($command);

        $timestamp = time();
        $event = '{"'.$timestamp.'":{"id":"'.$_SERVER["SSL_SESSION_ID"].'","type":"change","trip":"","message":""}}';
        event_write($timestamp, $event);
        event_post($event);
    }

    header("Location: index.php");
    // echo $jsondata;

    function remove_empty(&$array) {
        for ($i = count($array)-1; $i>=0; $i-=1) {
            if (empty($array[$i]["start"])) {
                array_splice($array, $i, 1);
            }
        }
    }

    function event_write($time, $event) {
        $date = date("Y-m-d", $time);

        $filename = "../../events/".$date;
        $file = fopen($filename, "a+") or die("Unable to open event file!");

        $data = $event."\n";

        fwrite($file, $data);
        fclose($file);
    }

    function event_post($event) {
        // Setup cURL
        $ch = curl_init('https://dubovozka-notification.herokuapp.com/store');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => $event
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die("Operation POST failed!\n".$response."\n".curl_error($ch)."\n".curl_getinfo($ch));
        }

        // Close cURL
        curl_close($ch);
    }
?>