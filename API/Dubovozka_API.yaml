openapi: 3.0.3
info:
  title: Dubovozka
  description: |-
    # by DADA
    Mobile application helps students from Dubki to get to university / city by combining the schedule of Dubki shuttle with public transport schedule.
    Also allows to book seats in shuttle.
    ## Authors:
      - Dmitry Ryabtsev
      - Anastasia Alekseeva
      - Dmitry Skrylnikov
      - Azamat Ismagulov
      - Aleksey Kudashkin
    ## Features:
    ### A Student can:
      - **Check the schedule.** Student can view actual bus schedule in both directions.
      - **Book a seat.** Student can book a seat in a bus leaving at a certain time.
      - **Set a notification.** Student can set a notification about a bus flight.
    ### A Driver can:
      - **Warn about an accident.** Driver can warn other users about an accident by pushing a button ‘Warning’.
    ### An Admin can:
    - **Edit the schedule.** Admin can edit the schedule with a configuration file or with a web-app user interface.
  contact:
    email: diryabtsev@edu.hse.ru
  license:
    name: BSD 3-Clause License
    url: https://opensource.org/licenses/BSD-3-Clause
  version: "0.2"
servers:
  - url: https://rashion.net/dubovozka
  - url: https://dubovozka-notification.herokuapp.com
tags:
  - name: booking
    description: book trips
  - name: schedule
    description: schedule view and edit
  - name: driver
    description: trip management
  - name: qr
    description: qr codes
  - name: notification
    description: notification service
paths:
  /booking/book/:
    post:
      tags:
        - booking
      summary: Book a seat
      description: Book a seat
      operationId: book
      parameters:
        - name: student_message
          in: query
          description: Book a seat
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/student_message'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
  /booking/cancel/:
    post:
      tags:
        - booking
      summary: Cancel booking
      description: Cancel booking
      operationId: cancel
      parameters:
        - name: student_message
          in: query
          description: Cancel a seat
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/student_message'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
  /booking/getbooked:
    get:
      tags:
        - booking
      summary: Get booked
      description: Get booked trips
      operationId: getBooked
      parameters:
        - name: id
          in: query
          description: Student ID
          required: true
          explode: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/trip'
        '400':
          description: operation failed
  /booking/getcrowd:
    get:
      tags:
        - booking
      summary: Get crowded info
      description: Get info on business of trips
      operationId: getCrowd
      parameters:
        - name: id
          in: query
          description: Student ID
          required: true
          explode: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/crowd'
        '400':
          description: operation failed
  /schedule/student/:
    get:
      tags:
        - schedule
      summary: Get student version of Schedule
      description: Get schedule version for students
      operationId: getStudentSchedule
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/schedule'
        '400':
          description: operation failed
  /schedule/admin/:
    get:
      tags:
        - schedule
      summary: Get Admin verion of Schedule
      description: Get schedule version for admins
      operationId: getAdminSchedule
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/schedule'
        '400':
          description: operation failed
      security:
        - admin_auth:
          - write:schedule
          - read:schedule
  /schedule/admin/write/:
    post:
      tags:
        - schedule
      summary: Write Schedule
      description: Write new version of schedule
      operationId: writeAdminSchedule
      parameters:
        - name: schedule
          in: query
          description: Write Schedule
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/schedule'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
      security:
        - admin_auth:
          - write:schedule
          - read:schedule
  /driver/starttrip/:
    post:
      tags:
        - driver
      summary: Start Trip
      description: Send a report that a trip started
      operationId: startTrip
      parameters:
        - name: drivers_message
          in: query
          description: Start Trip
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/drivers_message'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
      security:
        - driver_auth:
          - write:trip
          - read:trip
  /driver/endtrip/:
    post:
      tags:
        - driver
      summary: End Trip
      description: Send a report that a trip ended
      operationId: endTrip
      parameters:
        - name: drivers_message
          in: query
          description: End Trip
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/drivers_message'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
      security:
        - driver_auth:
          - write:trip
          - read:trip
  /driver/accident/:
    post:
      tags:
        - driver
      summary: Accident
      description: Send a report that there is an accident
      operationId: accident
      parameters:
        - name: drivers_message
          in: query
          description: Accident
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/drivers_message'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
      security:
        - driver_auth:
          - write:trip
          - read:trip
  /qr:
    post:
      tags:
        - qr
      summary: Create QR
      description: Create QR by studentID and trip 
      operationId: postQR
      parameters:
        - name: student id
          in: query
          description: Student ID
          required: true
          content:
            application/json:
              schema:
                type: string
        - name: trip id
          in: query
          description: Booked trip ID
          required: true
          content:
            application/json:
              schema:
                type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/qr'
        '400':
          description: operation failed       
  /qr/student:
    get:
      tags:
        - qr
      summary: Get student QRs
      description: Get all student's QR codes
      operationId: getStudentQR
      parameters:
        - name: student id
          in: query
          description: Student ID
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/qr'
        '400':
          description: operation failed
  /qr/trip:
    get:
      tags:
        - qr
      summary: Get trip QRs for driver
      description: Get trip QRs for driver
      operationId: getTripQR
      parameters:
        - name: trip i
          in: query
          description: Booked trip ID
          required: true
          content:
            application/json:
              schema:
                type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/qrs'
        '400':
          description: operation failed
  /notification/storedevice:
    post:
      tags:
        - notification
      summary: Store device ID
      description: Stores device ID after installation 
      operationId: storeDevice
      parameters:
        - name: device
          in: query
          description: User device
          required: true
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/device'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
  /notification/store:
    post:
      tags:
        - notification
      summary: Store event
      description: Stores event
      operationId: store
      parameters:
        - name: event
          in: query
          description: Event
          required: true
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/timestamp'
      responses:
        '200':
          description: successful operation
        '400':
          description: operation failed
components:
  schemas:
    schedule:
      type: object
      properties:
        To:
          type: array
          items:
            $ref: '#/components/schemas/days'
        From:
          type: array
          items:
            $ref: '#/components/schemas/days'
    days:
      type: object
      properties:
        Weekdays:
          type: array
          items:
            $ref: '#/components/schemas/stations'
        Saturday:
          type: array
          items:
            $ref: '#/components/schemas/stations'
        Sunday:
          type: array
          items:
            $ref: '#/components/schemas/stations'
    stations:
      type: object
      properties:
        Odintsovo:
          type: array
          items:
            $ref: '#/components/schemas/trips'
        Slavyansky:
          type: array
          items:
            $ref: '#/components/schemas/trips'
        Molodezhnaya:
          type: array
          items:
            $ref: '#/components/schemas/trips'
    trips:
      type: object
      properties:
        start:
          type: string
          example: "7:00"
          nullable: true
    trip:
      type: object
      properties:
        direction:
          type: string
          enum:
            - To
            - From
        day:
          type: string
          enum:
            - Weekdays
            - Saturday
            - Sunday
        station:
          type: string
          enum:
            - Odintsovo
            - Slavyansky
            - Molodezhnaya
        start:
          type: string
          example: "7:00"
      required:
        - direction
        - day
        - station
        - start
    drivers_message:
      type: object
      properties:
        driver_id:
          type: string
        type:
          type: string
          enum:
            - start
            - end
            - accident
        trip:
          type: array
          items:
            $ref: '#/components/schemas/trip'
        message:
          type: string
      required:
        - id
        - trip
        - type
    student_message:
      type: object
      properties:
        student_id:
          type: string
        type:
          type: string
          enum:
            - book
            - cancel
            - set
        trip:
          type: array
          items:
            $ref: '#/components/schemas/trip'
        message:
          type: string
      required:
        - id
        - trip
        - type
    qr:
      type: object
      properties:
        student:
          type: string
          description: Strudent ID
        trip:
          type: array
          items:
            $ref: '#/components/schemas/trip'
    qrs:
      type: object
      properties:
        qr:
          type: array
          items: 
            $ref: '#/components/schemas/qr'
    device:
      type: object
      properties:
        deviceID: 
          type: string
          description: deviceID from Firebase
        role:
          type: string
          enum:
              - driver
              - student
    timestamp:
      type: object
      properties:
        time:
          type: string
          description: timestamp
        event:
          type: array
          items:
            oneOf:
              - $ref: '#/components/schemas/student_message'
              - $ref: '#/components/schemas/drivers_message'
    events:
      type: array
      description: stores events
      items:
        $ref: '#/components/schemas/timestamp'
    crowd:
      type: object
      properties:
        trip:
          type: array
          items:
            $ref: '#/components/schemas/trip'
        busy:
          type: integer
          minimum: 1
          maximum: 30
  securitySchemes:
    admin_auth:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: https://rashion.net/dubovozka/admin/auth
          scopes:
            write:schedule: modify schedule
            read:schedule: read admin version of schedule
    driver_auth:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: https://rashion.net/dubovozka/driver/auth
          scopes:
            write:trip: write trip
            read:trip: read trip
