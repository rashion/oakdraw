# Dubovozka
### by DADA
---

Mobile application helps students from Dubki to get to university / city by combining the schedule of Dubki shuttle with public transport schedule. Also allows to book seats in shuttle.

## Authors:
- Dmitry Ryabtsev
- Anastasia Alekseeva
- Dmitry Skrylnikov
- Azamat Ismagulov
- Aleksey Kudashkin

## Features:

A Student can:
- **Check the schedule.** Student can view actual bus schedule in both directions.
- **Book a seat.** Student can book a seat in a bus leaving at a certain time.
- **Set a notification.** Student can set a notification about a bus flight.

A Driver can:
- **Warn about an accident.** Driver can warn other users about an accident by pushing a button ‘Warning’.

An Admin can:
- **Edit the schedule.** Admin can edit the schedule with a configuration file or with a web-app user interface.

## Similar systems:
Yandex Transport, hse.app.X, Google Calendar and analogues.
